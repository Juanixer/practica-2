/*
Copyright [2022] [Juan García-Obregón]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at:

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,2
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
either express or implied. See the License for the specific
language governing permissions and limitations under the
License.

*/


package mates;


public class Matematicas{

  /**
         * @author Juan García-Obregón
         * @version java 11.0.12
         *
         * @see a traves del metodo montecarlo se genera una aproximación a pi y se
         * crea un parametro que establecerá el numero de puntos/lanzamientos generado.
        */


   
     public static double generarNumeroPiIterativo(long pasos, long i, long puntosDentro){	        



	if(i <= pasos ){
	  double x = Math.random();
	  double y = Math.random();
	
	  if( Math.pow(x, 2) + Math.pow(y, 2) <= 1){
		puntosDentro++;
	  }
	 
	 return generarNumeroPiIterativo(pasos, i + 1, puntosDentro);

	}
	else{
	return 4*puntosDentro/(double)pasos;
	


	}



     }

	
     }

